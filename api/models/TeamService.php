<?php

include_once "TeamElement.php";

class TeamService {
    public $html;

    function __construct(TeamElement $teamElement){
        $this->html = '
            <div class="col-lg-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="assets/img/'.$teamElement->pictureItem->url.'" alt="'.$teamElement->pictureItem->alt.'" />
                    <h4>'.$teamElement->name.'</h4>
                    <p class="text-muted">'.$teamElement->function.'</p>'
        ;

        if($teamElement->gitLab != null){
            $this->html .= '<a class="btn btn-dark btn-social mx-2" href="'.$teamElement->gitLab.'" target="blanck"><i class="fab fa-git-square"></i></a>';
        }

        if($teamElement->webSite != null){
            $this->html .= '<a class="btn btn-dark btn-social mx-2" href="'.$teamElement->webSite.'" target="blanck"><i class="fas fa-globe-europe"></i></a>';
        }


        if($teamElement->twiter != null){
            $this->html .= '<a class="btn btn-dark btn-social mx-2" href="'.$teamElement->twiter.'" target="blanck"><i class="fab fa-twitter"></i></a>';
        }

        if($teamElement->linkedin != null){
            $this->html .= '<a class="btn btn-dark btn-social mx-2" href="'.$teamElement->linkedin.'" target="blanck"><i class="fab fa-linkedin-in"></i></a>';
        }

        return $this->html .= '
                </div>
            </div>
        ';
        
    }
}