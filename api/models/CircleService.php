<?php

class CircleService {
    public $html = '';


    function __construct(string $fontawesomeLogo, string $title, string $body){
        $this->html = '
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fas fa-circle fa-stack-2x text-primary"></i>
                    <i class="fas '.$fontawesomeLogo.' fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="my-3">'.$title.'</h4>
                <p class="text-muted">'.$body.'</p>
            </div>
        ';

        return $this->html;
    }
}