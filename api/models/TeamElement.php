<?php

include_once "api/models/PictureItem.php";

class TeamElement {
    public $pictureItem;
    public $name;
    public $function;
    public $twiter;
    public $webSite;
    public $linkedin;
    public $gitLab;

    function __construct(
        PictureItem $pictureItem,
        string $name,
        string $function,
        string $twiter,
        string $webSite,
        string $linkedin,
        string $gitLab
    ){
        $this->pictureItem = $pictureItem;
        $this->name = $name;
        $this->function = $function;
        $this->twiter = $twiter;
        $this->webSite = $webSite;
        $this->linkedin = $linkedin;
        $this->gitLab = $gitLab;

    }

}