<?php
include_once "api/models/PictureItem.php";
include_once "api/models/CardService.php";
include_once "api/models/CircleService.php";
include_once "api/models/TimeLineService.php";
include_once "api/models/TeamElement.php";
include_once 'api/models/TeamService.php';


class WebSite {
    public $cssFile;
    public $title;
    public $navBarLogo;
    public $headerSubMenu;
    public $headerMenu;
    public $headerButton;

    // navigation menu
    public $servicesMenu;
    public $portfolioMenu;
    public $aboutMenu;
    public $teamMenu;

    // Sections variables : by menu hooks
        // #services
        public $servicesSectionTitle;
        public $servicesSectionSubTitle;
        public $servicesSectionHtml;
        // #portfolio
        public $portfolioSectionTitle;
        public $portfolioSectionSubTitle;
        public $portfolioSectionHtml;
        // #about
        public $aboutSectionTitle;
        public $aboutSectionSubTitle;
        public $aboutSectionHtml;
        public $aboutSectionFinalCircle;

        // #team
        public $teamSectionTitle;
        public $teamSectionSubTitle;
        public $teamSectionHtml;
        public $teamSectionFinalText;

        // #contact
        // j'ai supprimé cette partie

    
    // Les variables sont dans l'ordre d'apparition du site, et par section
    function __construct(){
        $this->title = "Lucie.fr";
        $this->cssFile = "seb.css";
        $this->navBarLogo = "navbar-logo.svg";
        $this->headerSubMenu = "Soyez les bienvenus !";
        $this->headerMenu = "Chez Lucie";
        $this->headerButton = "À mon propos";

        // menu

        $this->servicesMenu = "MON PROFIL";
        $this->portfolioMenu = "Portfolio";
        $this->aboutMenu = "A mon propos";
        $this->teamMenu = "Collaborateurs";
        
        // #services
        $this->servicesSectionTitle = "MON PROFIL";
        $this->servicesSectionSubTitle = "De vendeur à développeur... de travail à passion...";
        $this->servicesSectionHtml = $this->serviceSectionMaker(); // remplir cette méthode
        
        // #portfolio
        $this->portfolioSectionTitle = "PORTFOLIO";
        $this->portfolioSectionSubTitle = "Dans ma quête de reconversion j'ai mené à bien un certain nombre de projets dont voici les principaux.";
        $this->portfolioSectionHtml = $this->portfolioSectionMaker(); // remplir cette méthode

        // #about
        $this->aboutSectionTitle = "Retour vers le futur";
        $this->aboutSectionSubTitle = "Je vais vous exposer rapidement mon cursus professionnel.";
        $this->aboutSectionFinalCircle = 'Je réalise<br>un rêve<br><i class="fas fa-cloud-sun"></i>';
        $this->aboutSectionHtml = $this->aboutSectionMaker(); // remplir cette méthode

        // #team
        $this->teamSectionTitle = "La dev team";
        $this->teamSectionSubTitle = "Uni par un seul crédo : 'Hello world !'";
        $this->teamSectionHtml = $this->teamSectionMaker(); // remplir cette méthode
        $this->teamSectionFinalText = "
        Mettre la farine dans une terrine et former un puits.
        Y déposer les oeufs entiers, le sucre, l'huile et le beurre.
        Mélanger délicatement avec un fouet en ajoutant au fur et à mesure le lait. La pâte ainsi obtenue doit avoir une consistance d'un liquide légèrement épais.
        Parfumer de rhum.
        Faire chauffer une poêle antiadhésive et la huiler très légèrement. Y verser une louche de pâte, la répartir dans la poêle puis attendre qu'elle soit cuite d'un côté avant de la retourner. Cuire ainsi toutes les crêpes à feu doux.
        ";


    }

    // array[CircleService]
    private function serviceSectionMaker(){
        $html = "";
        
        // faire autant de cercles que l'on veut pour la section mon profil mais par multiple de 3
        $circles = [
            new CircleService(
                "fa-cash-register",
                "Commerce",
                "Si ça a le goût du poulet, l'odeur du poulet et ça ressemble à du poulet, mais que Chuck Norris te dit que c'est du mouton, alors cherche pas, c'est du mouton."
            ),
        ];

        foreach ($circles as $circle) {
            $html .= $circle->html;
        }

        return $html;

    }

    // array[CardService]
    private function portfolioSectionMaker(){    

        $html = "";
        $modals = "";

        $cards = [
            new CardService(
                "1",
                "fa-hand-spock",
                new PictureItem("01-thumbnail.jpg","Fid'shop project picture"),
                "Fid'shop",
                "Projet d'initiation à DART",
                "Fid'shop",
                "Le système de fidélisation à destination des TPE",
                "Si les paramètres search et replace sont des tableaux, alors la fonction str_replace() prendra une valeur de chaque tableau et les utilisera pour la recherche et le remplacement sur subject. Si les paramètres replace a moins de valeurs que le paramètre search, alors une chaîne de caractères vide sera utilisée comme valeur pour le reste des valeurs de remplacement. Si le paramètre search est un tableau et que le paramètre replace est une chaîne de caractères, alors cette chaîne de caractères de remplacement sera utilisée pour chaque valeur de search. L'inverse n'a pas de sens. ",
                [
                    new PictureItem("01-full.jpg","test"),
                    new PictureItem("02-full.jpg","test"),
                    new PictureItem("03-full.jpg","test")
                ],
                [
                    "Participants : Lucie, Evan et moi",
                    "Langage : DART"
                ],
                "Fermer"
            )
        ];

        foreach ($cards as $card) {
            $html .= $card->html;
            $modals .= $card->modals;
        }

        return [
            "html"=>$html,
            "modals"=>$modals
        ];

    }

    // array[AboutService]
    private function aboutSectionMaker(){
        $html = "";
        $timelines = [
            new TimeLineService(
                new PictureItem("01-full.jpg","test"),
                "date",
                "titre",
                "description"
            )
        ];

        foreach($timelines as $index => $timeline){
            if($index % 2 != 0){
                $timeline->html = str_replace("###class###","timeline-inverted",$timeline->html);
            }
            $html .= $timeline->html;
        }

        $html .= '
        <li>
            <div class="timeline-image">
                <h4>
                    '.$this->aboutSectionFinalCircle.'
                </h4>
            </div>
        </li>';

        return $html;
    }

    private function teamSectionMaker(){

        $html= "";

        $team = [
            new TeamElement(
                // exemple avec moi mais ne le supprimez pas ;)
                new PictureItem("seb.jpeg","Sébastien Busschot"),
                "Sébastien Busschot",
                "Développeur full-stack, DevOps",
                "", // N°1 : Twiter n'affichera rien : je ne veux pas afficher twiter
                "http://busschot.fr/", // N°2 : site perso (metttez votre nom de domaine => redirection par cadre sur 5.196.95.21)
                "https://www.linkedin.com/in/sebastien-busschot/l",// N°3 : dois-je préciser? :D
                "https://gitlab.com/sebastien.pro.busschot"
            )
        ];

        foreach ($team as $element) {
            $staf = new TeamService($element);
            $html .= $staf->html;
        }

        return $html;
    }
}

