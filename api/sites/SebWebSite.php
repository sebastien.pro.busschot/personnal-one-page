<?php
require_once "api/models/PictureItem.php";
require_once "api/models/TeamElement.php";
require_once "api/models/CardService.php";
require_once "api/models/CircleService.php";
require_once "api/models/TimeLineService.php";
require_once 'api/models/TeamService.php';



class WebSite {
    public $cssFile;
    public $title;
    public $navBarLogo;
    public $headerSubMenu;
    public $headerMenu;
    public $headerButton;

    // navigation menu
    public $servicesMenu;
    public $portfolioMenu;
    public $aboutMenu;
    public $teamMenu;

    // Sections variables : by menu hooks
        // #services
        public $servicesSectionTitle;
        public $servicesSectionSubTitle;
        public $servicesSectionHtml;
        // #portfolio
        public $portfolioSectionTitle;
        public $portfolioSectionSubTitle;
        public $portfolioSectionHtml;
        // #about
        public $aboutSectionTitle;
        public $aboutSectionSubTitle;
        public $aboutSectionHtml;
        public $aboutSectionFinalCircle;

        // #team
        public $teamSectionTitle;
        public $teamSectionSubTitle;
        public $teamSectionHtml;
        public $teamSectionFinalText;

        // #contact

    
    function __construct(){
        $this->title = "Busschot.fr";
        $this->cssFile = "seb.css";
        $this->navBarLogo = "navbar-logo.svg";
        $this->headerSubMenu = "Soyez les bienvenus !";
        $this->headerMenu = "3, 2, 1, Programmez !";
        $this->headerButton = "À mon propos";
        
        // menu
        
        $this->servicesMenu = "MON PROFIL";
        $this->portfolioMenu = "Portfolio";
        $this->aboutMenu = "A mon propos";
        $this->teamMenu = "La team";
        
        // #services
        $this->servicesSectionTitle = "final seb = new dev(doc)";
        $this->servicesSectionSubTitle = "De vendeur à développeur... de travail à passion...";
        $this->servicesSectionHtml = $this->serviceSectionMaker();
        
        // #portfolio
        $this->portfolioSectionTitle = "PORTFOLIO";
        $this->portfolioSectionSubTitle = "Dans ma quête de reconversion j'ai mené à bien un certain nombre de projets dont voici les principaux.";
        $this->portfolioSectionHtml = $this->portfolioSectionMaker();

        // #about
        $this->aboutSectionTitle = "Retour vers le futur";
        $this->aboutSectionSubTitle = "Je vais vous exposer rapidement mon cursus professionnel.";
        $this->aboutSectionFinalCircle = 'Une passion<br>est née<br><i class="fas fa-code"></i>';
        $this->aboutSectionHtml = $this->aboutSectionMaker();

        // #team
        $this->teamSectionTitle = "La dev team";
        $this->teamSectionSubTitle = "Uni par un seul crédo : 'Hello world !'";
        $this->teamSectionHtml = $this->teamSectionMaker();
        $this->teamSectionFinalText = "
        La dream team, unis autours de la même passion : coder. Chaqu'un apportant sa pierre à l'édifice, venant élever le niveau général de par son point de vue spécifique.
        <br>
        <br>Lucie, l'artiste du groupe, qui mange du CSS au petit déjeuner et dinne de l'API.
        <br>
        <br>Evan, le seigneur Sith, armé de ses obscures tableaux. Il mene la vie dure aux problèmes qu'il affronte sans...heu...problème.
        <br>
        <br>Volontaires et percévérants, j'ai grand plaisir à travailler en équipe avec eux !
        ";


    }

    // array[CircleService]
    private function serviceSectionMaker(){
        $html = "";
        
        
        $circles = [
            new CircleService(
                "fa-cash-register",
                "Précédement",
                "Plongé depuis toujours dans le bain du commerce, j'ai passé un BTS en management puis je suis entré dans la vie active. Manutentionaire, hôte d'accueil en SAV, vendeur, responsable puis manageur. J'ai prouvé mon éfficacité et mon investissement sur des postes très variés."
            ),
            new CircleService(
                "fas fa-recycle",
                "Reconversion",
                "En 2018 j'étais à la tête d'un commerce depuis 3 ans et je sentais le besoin de faire autre chose.
                <br><br>J'ai pas mal tatonné, cherché et finalement trouvé !
                <br><br>En autodidacte puis dans une école \"piscine\" j'ai découvert le code et la passion qui va avec !
                "
            ),
            new CircleService(
                "fa-project-diagram",
                "Projets",
                "Actuellement je vise un master en conception logicielle. Je souhaite devenir architecte logiciel et coach agile au sein d'une équipe qui partage la même passion pour l'IT et les nouvelles technologies.
                <br><br>En ce moment je suis en Bachelor 3 DevOps chez EPSI Montpellier."
            ),
        ];

        foreach ($circles as $circle) {
            $html .= $circle->html;
        }

        return $html;

    }

    // array[CardService]
    private function portfolioSectionMaker(){    

        $html = "";
        $modals = "";

        $cards = [
            new CardService(
                "adminFidShop",
                "fa-hand-spock",
                new PictureItem("admin_fidshop.jpg","Fid'shop project picture"),
                "Fid'shop",
                "Projet d'initiation à DART",
                "Fid'shop",
                "(WORK IN PROGRESS) Le système de fidélisation à destination des TPE",
                "Après avoir mangé un bon burger j'ai vu le commerçant écrire mon passage sur un bristol de fidélisation. Le projet était né, développer un écosystème gestion de carte de fidélisation.
                <br><br>Il ne sagit que d'un prétexte pour découvrir DART, le nouveau langage de GOOGLE.
                <br><br>Imaginé sous la forme de 2 applications mobiles Flutter et d'une application web Flutter-beta reposant sur une API Aqueduct. Une approche full-stack et peutêtre notre première publication d'application mobile ?
                <br><br>Projet en cours...",
                [
                    new PictureItem("fidshopApi.png","Fid'shop project picture"),

                ],
                [
                    "Participants : Lucie, Evan et moi",
                    "Langage : DART"
                ],
                "Fermer"
            ),
            new CardService(
                "jlassureAdmin",
                "fa-hand-spock",
                new PictureItem("jlassure.png","jlassure project picture"),
                "CMS JLAssure",
                "Projet de stage sous CodeIgniter",
                "Espace d'administration JLAssure",
                "Un CMS maison sous PHP/JS",
                "À la demande de l'entreprise nous avons conçu et développé une solution permettant de génèrer plusieurs sites sur un même serveur, administrés depuis une interface et gèrant l'envoi de mails automatiques.
                <br><br>La génération des pages web se fait en front grâce à un JSON provennant d'une API.
                <br><br>Ce projet a été présenté lors de ma <a href=\"https://docs.google.com/document/d/1p-b5qTCJdnGVDR_r_o0GoZBNWbSiny-RuEy3CazcqVY/edit?usp=sharing\" target='blanck'>soutenance</a> pour le titre pro de développeur web et web mobile.",
                [
                    new PictureItem("jlassure.png","jlassure project picture"),

                ],
                [
                    "Participants : <a href=\"https://www.linkedin.com/in/paulinericard/\" target='blanck'>Pauline</a> et moi",
                    "Langage : PHP/JS",
                    "FrameWorks / Librairies : CodeIgniter, Gridstack, JQuery"
                ],
                "Fermer"
            ),
            new CardService(
                "jlassureApi",
                "fa-hand-spock",
                new PictureItem("jlassure_api.png","jlassure api project picture"),
                "API JLAssure",
                "Projet de stage sous CodeIgniter",
                "API JLAssure",
                "Une API REST sous CodeIgniter",
                "Nous avons conçu et développé cette API pour qu'elle serve de support au projet de CMS de JLAssure. L'API, adaptée pour être REST, est en capacité de recevoir une page déstructurée et de la rendre restituée tout en évitant les doublons grâce à un pérénnisation incrémentielle des données liées aux pages.
                <br><br>Elle est aussi en mesure d'émetre des mails, précédement enregistrés en HTML, à la suite d'un évenement.
                <br><br>Ce projet a été présenté lors de ma <a href=\"https://docs.google.com/document/d/1p-b5qTCJdnGVDR_r_o0GoZBNWbSiny-RuEy3CazcqVY/edit?usp=sharing\" target='blanck'>soutenance</a> pour le titre pro de développeur web et web mobile.
                ",
                [
                    new PictureItem("jlassure_api.png","jlassure api project picture"),

                ],
                [
                    "Participants : <a href=\"https://www.linkedin.com/in/yoan-cougnaud-61278a189//\" target='blanck'>Yoan</a> et moi",
                    "Langage : PHP",
                    "FrameWorks : CodeIgniter"
                ],
                "Fermer"
            ),
            new CardService(
                "ENA",
                "fa-hand-spock",
                new PictureItem("lena.jpg","lena project picture"),
                "ENA",
                "Projet externe sous Symfony",
                "Prototype pour une la startup ENA",
                "Un triporteur pour lutter contre la fracture du numérique",
                "
                ",
                [
                    new PictureItem("lena.jpg","lena project picture"),

                ],
                [
                    "Participants : <a href=\"https://www.linkedin.com/in/sebastien-gil-90aa5a188/\" target='blanck'>Sébastien</a>, <a href=\"https://www.linkedin.com/in/florian-marco-8baab235/\" target='blanck'>Florian</a>, <a href=\"https://www.linkedin.com/in/lucie-barthes/\" target='blanck'>Lucie</a> et moi",
                    "Langages : PHP - JavaScript - Twig",
                    "FrameWorks : Symfony / JQuery"
                ],
                "Fermer"
            ),
            new CardService(
                "Frameworkphp",
                "fa-hand-spock",
                new PictureItem("mvcBwb.jpg","Framework php"),
                "BeWeb framework",
                "Framework maison sous PHP",
                "BeWeb framework",
                "Un petit framework de A à Z",
                "Dans le but de comprendre le principe de fonctionnement d'un framework nous en avons développé un grâce au modèle MVC. Il disposait également d'un ORM simplifié pour la création des modèles.",
                [
                    new PictureItem("bewebframework.png","Framework php"),
                    
                ],
                [
                    "Langage : PHP"
                ],
                "Fermer"
            ),
            new CardService(
                "formenscop",
                "fa-hand-spock",
                new PictureItem("formenscop.png","Formenscop"),
                "FormEnScop",
                "Projet interne chez BeWeb",
                "FormEnScop : Espace Salarié",
                "Un template alimenté par une API REST",
                "Ce projet ne m'est plus accéssible mais reposait sur un template bootstrap similaire à celui présenté.
                <br>Le vrai intérêt de ce site c'est qu'il repose sur un framework maison et une api. Le template étant alimenté par de l'AJAX en JQuery.
                <br>Notre mission consistait à développer un site a destination des salaries de FormesnScop. Il dispose des fonctionnalités suivantes :
                <ul>
                <li>Le salarié est formateur, il peut afficher / modifier la feuille de présence du jour et visionner les précédentes.</li>
                <li>Un chat websocket est disponible et permet de communiquer avec les étudiants, les autres formateurs et la RH.</li>
                <li>Le formateur dispose d'une bibliothèque de fichiers qu'il met à disposition des étudiants de sa formation. Le filesystem est calqué sur celui de Linux.</li>
                <li>L'application est protégèe par un token stocké dans un cookie et passée dans le header.</li>
                </ul>",
                [
                    new PictureItem("fakeadmin.png","Fake admin page"),

                ],
                [
                    "Langage : PHP / JAVASCRIPT",
                    "FrameWorks : BeWeb, précédent TP"
                ],
                "Fermer"
            ),
            new CardService(
                "oldsite",
                "fa-hand-spock",
                new PictureItem("sebsite.png","Vieux site"),
                "Site personnel 1.0",
                "Projet personnel MVC",
                "busschot.fr 1.0",
                "Refonte de mon site en MVC",
                "Servant de sandbox, j'y ai fait plein de tests, crash et éxpériences. Le site m'a permis d'acceder à un cloud personnel, de regarder en streaming le contenu de mon choix, d'informer mon petit cousin de la disponibilité du serveur mincraft, de faire des sondages concernant la naissance de mon fils, etc...",
                [
                    new PictureItem("sebsite.png","Vieux site"),

                ],
                [],
                "Rester (lol)"
            ),
            new CardService(
                "git",
                "fa-hand-spock",
                new PictureItem("gitCurse.png","git curse"),
                "Bash curse",
                "Initiation au scripting bash et git",
                "Initiation git / bash",
                "L'occasion d'apprendre le scripting et de partager son expérience",
                "Je me suis rendu compte que beaucoup de mes cammarades avaient du mal avec git et qu'aucun n'avait fait de script sous bash. Je me suis mis en tête de faire un cours initiatique sachant que je n'y connaissait rien au départ.
                <br>J'ai donc fait mes recherches et conçu un assistant très basique permettant d'executer les commandes git les plus utilisées.
                <br>En parallèle j'ai fait un document texte servant de support pédagogique à l'issue de quoi les participants devaient réaliser leur propre script.
                <br>J'ai été chaudement accueilli, un beau moment de partage !",
                [
                    new PictureItem("bashCurse.png","bash curse"),

                ],
                [
                    "Langage : Bash",
                    "Intérréssé? <a href=\"https://gitlab.com/sebastien.pro.busschot/framework-mvc-bwb\" target='blanck'>Dépot git</a>"
                ],
                "Fermer"
            ),
            new CardService(
                "perso2",
                "fa-hand-spock",
                new PictureItem("perso2.jpg","Nouveau site"),
                "One page",
                "Un portfolio multi-utilisateur",
                "Le portfolio caméléon",
                "Une structure se remplissant différemment en fonction du nom de domaine demandeur",
                "Conçu en 2 jours pour répondre au besoin urgent de faire un portfolio je me suis mis en tête d'adapter un template bootstrap avec une classe représentant le site.
                <br><br>Après réflection je me suis dit qu'avec peu de code il pouvait servir à plusieures personnes.
                <br>Je suis parti d'un routing très simple qui appelle le fichier de \"remplissage\" correspondant au nom de domaine.
                <br><br>Vous voulez tester? Mais vous y êtes ! (Allez voir les sites de la team en bas de la page)",
                [
                    new PictureItem("perso2.jpg","Nouveau site"),
                ],
                [
                    "Langage : PHP",
                    "Le code vous intérresse? <a href=\"https://gitlab.com/sebastien.pro.busschot/personnal-one-page\" target='blanck'>Depot Git</a>"
                ],
                "Fermer"
            ),
            new CardService(
                "aristo",
                "fa-hand-spock",
                new PictureItem("lesaristo.jpg","Les aristo"),
                "Site vitrine",
                "Un petit cadeau pour un bon resto",
                "Un site de présentation",
                "Sous CodeIgniter 4, simple et utilisable sans code",
                "Afin de donner un petit coup de pouce à la belle-famille et à leur restaurant, je me suis donné comme projet de leur faire un site vitrine. Je me suis donné une semaine.
                <br>L'objectif était de proposer un site sans BDD mais personnalisable sans dev. Je suis donc passé par un JSON pour le stockage et un modèle PHP pour vérifier son intégrité.",
                [
                    new PictureItem("lesaristo2.jpg","Les aristo"),
                ],
                [
                    "Langage : PHP / JS",
                    "Site pas encore passé en production"
                ],
                "Fermer"
            ),
            new CardService(
                "after a",
                "fa-hand-spock",
                new PictureItem("after.png","after.png"),
                "...",
                "Patiente ça arive...",
                "Le portfolio caméléon",
                "Une structure se remplissant différemment en fonction du nom de domaine demandeur",
                "Conçu en 2 jours pour répondre au besoin urgent de faire un portfolio je me suis mis en tête d'adapter un template bootstrap avec une classe représentant le site.
                <br><br>Après réflection je me suis dit qu'avec peu de code il pouvait servir à plusieures personnes.
                <br>Je suis parti d'un routing très simple qui appelle le fichier de \"remplissage\" correspondant au nom de domaine.
                <br><br>Vous voulez tester? Mais vous y êtes ! (Allez voir les sites de la team en bas de la page)",
                [
                    new PictureItem("lesaristo2.jpg","Les aristo"),
                ],
                [
                    "Langage : PHP",
                    "Le code vous intérresse? <a href=\"https://gitlab.com/sebastien.pro.busschot/personnal-one-page\" target='blanck'>Depot Git</a>"
                ],
                "Fermer"
            ),
            new CardService(
                "after a",
                "fa-hand-spock",
                new PictureItem("after.png","after.png"),
                "...",
                "Patiente ça arive...",
                "Le portfolio caméléon",
                "Une structure se remplissant différemment en fonction du nom de domaine demandeur",
                "Conçu en 2 jours pour répondre au besoin urgent de faire un portfolio je me suis mis en tête d'adapter un template bootstrap avec une classe représentant le site.
                <br><br>Après réflection je me suis dit qu'avec peu de code il pouvait servir à plusieures personnes.
                <br>Je suis parti d'un routing très simple qui appelle le fichier de \"remplissage\" correspondant au nom de domaine.
                <br><br>Vous voulez tester? Mais vous y êtes ! (Allez voir les sites de la team en bas de la page)",
                [
                    new PictureItem("lesaristo2.jpg","Les aristo"),
                ],
                [
                    "Langage : PHP",
                    "Le code vous intérresse? <a href=\"https://gitlab.com/sebastien.pro.busschot/personnal-one-page\" target='blanck'>Depot Git</a>"
                ],
                "Fermer"
            ),
        ];

        foreach ($cards as $card) {
            $html .= $card->html;
            $modals .= $card->modals;
        }

        return [
            "html"=>$html,
            "modals"=>$modals
        ];

    }

    // array[AboutService]
    private function aboutSectionMaker(){
        $html = "";
        $timelines = [
            new TimeLineService(
                new PictureItem("cravatte.jpg","Commerce"),
                "2000-2018",
                "Commerce",
                "Vendeur / Magasinier / Responsable : J'ai changé régulièrement d'entreprises et aquis une bonne expérience dans ce domaine. Je m'y plaisait mais il manquais quelque chose..."
            ),
            new TimeLineService(
                new PictureItem("linux.png","Linux logo"),
                "2016-2018",
                "GNU/Linux Sysadmin",
                "Habitué à Windows et poussé par la curiosité je me suis penché sur le cas de l'open source et Linux en particulier. Ce que j'y ai découvert à peu à peu changé ma façon d'aborder l'informatique. Du HTTP, au réseau, à l'administration puis au scripting. Plus j'en découvrait, plus j'avais envie de creuser..."
            ),
            new TimeLineService(
                new PictureItem("csharp.png","c#"),
                "2018",
                "Premier langage",
                "Faut être honnête, j'ai rien compris ¯\_(ツ)_/¯. Ses histoires d'objet personne dérivé de vivant m'embrouillaient complétement. Je pense que commencer par la POO en solo n'etait pas la méilleure idée. Cela etant, j'ai beaucoup apprécié d'avoir à résoudre des problemes d'algorithmie."
            ),
            new TimeLineService(
                new PictureItem("idee.jpg","Idee"),
                "Fin 2018",
                "Reconversion",
                "OpenClassrooms, w3schools, Sololearn... J'ai commencé ma reconversion en autodidacte."
            ),
            new TimeLineService(
                new PictureItem("web.jpg","Web technos"),
                "2018-2019",
                "Autoformation aux langages du web",
                "En m'appuyant sur les MOOC sités précédement j'ai commencé à apprendre à coder. Cela à confirmé mes choix et attisé ma motivation !"
            ),
            new TimeLineService(
                new PictureItem("beweb.png","BeWeb"),
                "2019",
                "BeWeb : l'école de l'apprentissage !",
                "BeWeb est une école \"42\", c'est à dire que l'apprentissage se fait sans cours, par la pratique et l'entre-aide. Les projets variés mont permis de m'initier aux technologies les plus en vogue. Une belle expérience !"
            ),
            new TimeLineService(
                new PictureItem("php.png","Php logo"),
                "2019",
                "PHP, débugage et algorithmique",
                "Premiers algorithmes sous PHP, sous l'impulsion du formateur j'ai commencé à voir mon code différement. C'est la partie que je préfère à ce jour, l'algorithmique."
            ),
            new TimeLineService(
                new PictureItem("javascript.png","JS logo"),
                "2019",
                "JS / JQuery",
                "Découverte de l'AJAX, dynamisation des pages et génération/modification de DOM. J'ai eu du mal avec cette dernière partie mais en percévérant j'y ai pris beacoup de plaisir."
            ),
            new TimeLineService(
                new PictureItem("poo.jpg","POO"),
                "2019",
                "Programmation Objet",
                "Je crois que c'est ce qui me caractérise aujourd'hui. J'aime organiser mon code grâce à la POO. C'est clair, logique et maintenable. Cela me donne envie de découvrir les designs patterns."
            ),
            new TimeLineService(
                new PictureItem("nodejs.png","Nodejs"),
                "2019",
                "Javascript en back",
                "J'ai beaucoup aimé le côté minimaliste de nodejs et son principe de fonctionnement asynchrone. Avec Express et l'ES6 c'est une technologie que j'apprécie pour sa légèreté et sa simplicité."
            ),
            new TimeLineService(
                new PictureItem("front.png","Framework front"),
                "2019",
                "Les framework front",
                "Les sacré saints framework JS pour le front. Après avoir réalisé une découverte sur Vue, React et Angular mon dévolu c'est jetté sur Angular pour sa structure. L'autre argument pour ce choix est qu'Ionic, qui se base sur Angular, permet de faire des applications mobiles."
            ),
            new TimeLineService(
                new PictureItem("Java-or-Kotlin.jpg","java et kotlin"),
                "2019",
                "Le développement mobile",
                "La redécouverte de l'orienté Objet. Déstabilisant au premier contact, la structure du langage JAVA/Kotlin m'a énormément servi pour la suite."
            ),
            new TimeLineService(
                new PictureItem("flutter.png","Flutter"),
                "2019",
                "Retour sur le mobile",
                "Nouveau langage, énorme potentiel ! DART !
                <br>100% orienté objet
                <br>Applis natives iOS/Android grâce à Flutter
                <br>Permet de faire du back avec Aqueduct
                <br>Multiplateforme applications desktop
                <br>Une belle découverte qui m'en apprend beaucoup !"
            )
        ];

        foreach($timelines as $index => $timeline){
            if($index % 2 != 0){
                $timeline->html = str_replace("###class###","timeline-inverted",$timeline->html);
            }
            $html .= $timeline->html;
        }

        $html .= '
        <li>
            <div class="timeline-image">
                <h4>
                    '.$this->aboutSectionFinalCircle.'
                </h4>
            </div>
        </li>';

        return $html;
    }

    private function teamSectionMaker(){

        $html= "";

        $team = [
            new TeamElement(
                new PictureItem("evan.jpeg","Evan Martinez Photo"),
                "Evan Martinez",
                "Developpeur full-stack",
                "",
                "http://developpeur-beziers.cf/",
                "https://www.linkedin.com/in/martinezevan/",
                "https://gitlab.com/evan34"
            ),
            new TeamElement(
                new PictureItem("lucie.jpeg","Lucie Barthes Photo"),
                "Lucie Barthes",
                "Developpeur full-stack",
                "",
                "http://www.lucie-barthes.tk/",
                "https://www.linkedin.com/in/lucie-barthes/",
                "https://gitlab.com/Luccia"
            ),
            new TeamElement(
                // exemple avec moi mais ne le supprimez pas ;)
                new PictureItem("seb.jpeg","Sébastien Busschot"),
                "Sébastien Busschot",
                "Développeur full-stack, DevOps",
                "", // N°1 : Twiter n'affichera rien : je ne veux pas afficher twiter
                "http://busschot.fr/", // N°2 : site perso (metttez votre nom de domaine => redirection par cadre sur 5.196.95.21)
                "https://www.linkedin.com/in/sebastien-busschot/l",// N°3 : dois-je préciser? :D
                "https://gitlab.com/sebastien.pro.busschot"
            )
        ];

        foreach ($team as $element) {
            $staf = new TeamService($element);
            $html .= $staf->html;
        }

        return $html;
    }
}

