<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="A simple personnal site used for a rapid presentation" />
        <meta name="author" content="Created whith a bootstrap template by dev.busschot" />
        <title><?php echo($webSite->title); ?></title>
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/<?php echo($webSite->cssFile); ?>" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top navbar-shrink" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="assets/img/<?php echo($webSite->navBarLogo); ?>" alt="" /></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ml-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto">
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="/">Busschot.fr</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#mediateque">Mediateque</a></li>
                        <?php
                            if(isset($_SESSION["role"]))
                            {
                                if($_SESSION["role"] != "user")
                                {
                                    ?>
                                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#transmission">Transmission</a></li>
                                    <?php
                                }
                            }
                        ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <div class="masthead-subheading"><?php echo($webSite->headerSubMenu); ?></div>
                <div class="masthead-heading text-uppercase"><?php echo($webSite->headerMenu); ?></div>
                <?php
                    if(!isset($_SESSION["role"])){
                        ?>
                            <form target="/prived" method="post">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                                </div>
                                <button type="submit" class="btn btn-primary">Se connecter</button>
                            </form>
                        <?php
                    } else {
                        ?>
                            <form target="/prived" method="post">
                                <input type="hidden" class="form-control" id="exampleInputPassword1" name="disconnect">
                                <button type="submit" class="btn btn-primary">Déconnexion</button>
                            </form>
                        <?php
                    }
                ?>
            </div>
        </header>

        <?php
            if(isset($_SESSION["role"])){
        ?>
        
            <!-- Services-->
            <section class="page-section row" id="yggTorrent">
            
                <h2 class="col-12" style="text-align: center;">Url d'yggtorrent</h2>
                <button id="yygBtn" class="btn btn-dark col-12"><?php echo $yggUrl ?></button>

            </section>
            <!-- Portfolio Grid-->
            <section class="page-section bg-light container-fluid" id="mediateque">

                <div class="row container-fluid">
                    <input class="col-12 col-md-9" type="text" id="searchInput">
                    <select class="col-12 col-md-3" id="searchSelect">
                        <option value="All">Tout afficher</option>
                    </select>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Nom</th>
                            <th scope="col">Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach ($files as $key => $file) // $file == ["fileName" => ["path","ext"], ...]
                        {
                            ?>
                                <tr class="filterMedia">
                                    <th scope="row"></th>
                                    <td><a href="<?php echo $file["path"] ?>" download><?php echo $key ?></a></td>
                                    <td><?php echo strtolower($file["ext"]) ?></td>
                                </tr>
                                <?php
                        } 
                        ?>
                        </tbody>
                </table>

            </section>
                <!-- Team-->
            <?php
                if(isset($_SESSION["role"]))
                {
                    if($_SESSION["role"] != "user")
                    {
                        ?>
                            <section class="page-section bg-light" id="transmission">

                                <iframe src="http://5.196.95.21:9091/transmission/web/" frameborder="0" width="100%" height="1000vh"></iframe>
                                    
                            </section>
                        <?php
                    }
                }
            ?>
            
        <?php
            }
        ?>
            
        <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 text-lg-left">Copyright © busschot.fr 2020</div>
                    <div class="col-lg-4 my-3 my-lg-0">
                        <!-- <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-twitter"></i></a> -->
                        <a class="btn btn-dark btn-social mx-2" href="https://www.facebook.com/Seb.busschot" target="blank"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="https://www.linkedin.com/in/sebastien-busschot/" target="blank"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                    <!-- <div class="col-lg-4 text-lg-right">
                        <a class="mr-3" href="#!">Privacy Policy</a>
                        <a href="#!">Terms of Use</a>
                    </div> -->
                </div>
            </div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Contact form JS-->
        <script src="assets/mail/jqBootstrapValidation.js"></script>
        <script src="assets/mail/contact_me.js"></script>
        <!-- Core theme JS-->
        <script
			  src="https://code.jquery.com/jquery-3.5.1.min.js"
			  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
              crossorigin="anonymous">
        </script>
        <script src="js/secret.js"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
