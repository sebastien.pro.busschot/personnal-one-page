<?php
session_start();

function getFiles($dir, $parent){
    $currentDir = $parent."/".$dir;
    if (is_dir($currentDir)) {
        $files = [];
        if ($dh = opendir($currentDir)) {
            while (($file = readdir($dh)) !== false) {
                $ignore = [".", ".."];
                $typeIgnore = ["nfo"];
                $pathinfo = pathinfo($currentDir."/".$file);
                if(!in_array($file, $ignore) && !in_array($pathinfo["extension"], $typeIgnore))
                {
                    if(is_dir($currentDir."/".$file))
                    {
                        $files = array_merge($files,getFiles($file, $currentDir));
                    } else {
                        $files[$pathinfo["filename"]] = [
                            "path" => $currentDir."/".$file,
                            "ext" => $pathinfo["extension"]
                        ];
                    }
                }
            }
            closedir($dh);
        }
        return $files;
    }
    return false;
}

if($_SERVER["HTTP_HOST"][0] == "w" && $_SERVER["HTTP_HOST"][1] == "w" && $_SERVER["HTTP_HOST"][2] == "w"){
    $host = str_replace("www.", "", $_SERVER["HTTP_HOST"]);
} else {
    $host = $_SERVER["HTTP_HOST"];
}

switch ($host) {
    case 'lucie-barthes.tk':
        require_once "api/sites/LucieWebSite.php";
    break;
    
    case 'developpeur-beziers.cf':
        require_once "api/sites/EvanWebSite.php";
    break;
        
    default:
        require_once "api/sites/SebWebSite.php";
    break;
    }

    $webSite = new WebSite();

    switch ($_SERVER["REQUEST_URI"]) {
        case '/prived':
            if(isset($_POST["password"])){
                if($_POST["password"] == "mellon"){
                    $_SESSION["role"] = "user";
                } elseif ($_POST["password"] == "arthur") { // si tu est sur git ne regarde pas ^^ , en fait je m'en moque
                    $_SESSION["role"] = "admin";
                }
            }
            
            if(isset($_POST["disconnect"])){
                session_destroy();
                header('Location: /prived');
            }

            $ygg = file_get_contents("https://t.me/yggtorrent");
            preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $ygg, $match);
            $yggUrl;
            foreach($match[0] as $url){
                if(strpos($url, "yggtorrent")){
                    $yggUrl = $url;
                    break;
                }
            }

            $files = getFiles("files", "assets");

            require_once "views/secret.php";
            break;
        
        default:
            require_once "views/homePage.php";
            break;
    }
?>
