$().ready(function(){

    $("#searchInput").on("keyup", function () {
        const val = $(this).val().toLowerCase();
        $(".filterMedia").each(function() {
            const text = this.children[1].innerText.toLowerCase();
            if(text.search(val) == -1){
                $(this).hide();
            } else {
                $(this).show();
            }
        })

        $("#searchSelect").val("All");
    })

    //<option value="volvo">Volvo</option>

    let extentions = [];
    $(".filterMedia").each(function() {
        const ext = this.children[2].innerText.toLowerCase();
        if(extentions.indexOf(ext) == -1){
            extentions.push(ext); 
        }

    })
    
    for (const ext of extentions) {
        $("#searchSelect").append(`<option value="${ext}">${ext}</option>`);
    }

    $("#searchSelect").on("change", function() {
        const val = $(this).val();
        $(".filterMedia").each(function() {
            const text = this.children[2].innerText;
            if(text.search(val) == -1 && val != "All"){
                $(this).hide();
            } else {
                $(this).show();
            }

            $("#searchInput").val("");

        })
    })

    $("#yygBtn").on("click", function() {
        window.open($(this).text(), '_blank');
    })

})